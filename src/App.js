import { 
  Routes, 
  BrowserRouter as Router, 
  Route,
  Link
} from 'react-router-dom'

import './app.scss'
import homeIcon from './assets/home-icon.png'
import Bootstrap from './pages/bootstrap/Bootstrap'
import Home from './pages/home/Home'
import Material from './pages/material-ui/Material'
import Html from './pages/html/Html'
import NotFound from './pages/page-not-found/NotFound'

const App = () => {
  return (
    <div className='main'>
      <Router>
        <nav>
          <Link className='nav-home' to='/'>
            <img 
              src={homeIcon}
              alt='Home-icon'
            ></img>
          </Link>
          <Link className='nav-link' to='/html'>Basic HTML</Link>
          <Link className='nav-link' to='/bootstrap'>Bootstrap</Link>
          <Link className='nav-link' to='/materialui'>Material UI</Link>
        </nav>
        <Routes>
          <Route path='/' element={<Home/>}></Route>
          <Route path='/html' element={<Html/>}></Route>
          <Route path='/bootstrap' element={<Bootstrap/>}></Route>
          <Route path='/materialui' element={<Material/>}></Route>
          <Route path='*' element={<NotFound/>}></Route>
        </Routes>
        <footer></footer>
      </Router>
    </div>
  )
}

export default App;