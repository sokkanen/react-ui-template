import { useState } from 'react'

const showPriceInEuroString = (price) => {
    const euros = Math.floor(price / 100)
    const cents = price % 100 > 10 ? `${price % 100}` : `0${price % 100}`
    return `${euros}.${cents}`
}

const initialItems = [
    { name: 'socks', price: 300, category: 'clothing' },
    { name: 'boots', price: 990, category: 'clothing' },
    { name: 'TV', price: 21000, category: 'electronics' }
]

const Initialcategories = [
    'clothing',
    'electronics'
]

const CategoryForm = ({ handleAddCategory }) => {
    return (
        <>
        <h2>Add a new category</h2>
        <form onSubmit={handleAddCategory}>
            <label htmlFor='name'>Category: </label>
            <input type='text' id='name'></input>
            <input type='submit'></input>
        </form>
        </>
    )
}

const ItemForm = ({ handleAddItem, categories }) => {
    return (
        <>
        <h2>Add a new item</h2>
        <form onSubmit={handleAddItem}>
            <label htmlFor='name'>Name: </label>
            <input type='text' id='name'></input>
            <label htmlFor='price'>Price (in cents): </label>
            <input type='number' id='price'></input>
            <label htmlFor='categories'>Category</label>
            <select id="category">
                {categories.map((c) => {
                    return <option key={c}>{c}</option>
                })}
            </select>
            <input type='submit'></input>
        </form>
        </>
    )
}

const Html = () => {
    const [items, setItems] = useState(initialItems)
    const [categories, setCategories] = useState(Initialcategories)

    const handleAddCategory = (event) => {
        event.preventDefault()
        const newCategory = event.target.name.value
        event.target.name.value = ''
        setCategories((categories) => {
            return [...categories, newCategory]
        })
    }

    const handleAddItem = (event) => {
        event.preventDefault()
        const newItem = {
            name: event.target.name.value,
            price: event.target.price.value,
            category: event.target.category.value
        }
        event.target.name.value = ''
        event.target.price.value = null
        setItems((items) => {
            return [...items, newItem]
        })
    }

    return (
        <div style={{ position: 'absolute', top: '200px'}}>
        <h1>Hello HTML Storage management</h1>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Category</th>
                </tr>
            </thead>
            <tbody>
                {items.map((item) => {
                    return <tr key={item.name}>
                        <td>{item.name}</td>
                        <td>{showPriceInEuroString(item.price)}</td>
                        <td>{item.category}</td>
                    </tr>
                })}
            </tbody>
        </table>
        <CategoryForm handleAddCategory={handleAddCategory}/>
        <ItemForm handleAddItem={handleAddItem} categories={categories}></ItemForm>
        </div>
    )
}

export default Html